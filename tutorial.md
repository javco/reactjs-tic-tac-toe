



Cada componente de React está encapsulado y puede operar independientemente;
esto te permite construir IUs complejas desde componentes simples.